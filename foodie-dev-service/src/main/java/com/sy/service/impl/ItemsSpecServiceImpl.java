package com.sy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.pojo.ItemsSpec;
import com.sy.service.ItemsSpecService;
import com.sy.mapper.ItemsSpecMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【items_spec(商品规格 每一件商品都有不同的规格，不同的规格又有不同的价格和优惠力度，规格表为此设计)】的数据库操作Service实现
* @createDate 2021-11-03 22:10:41
*/
@Service
public class ItemsSpecServiceImpl extends ServiceImpl<ItemsSpecMapper, ItemsSpec>
    implements ItemsSpecService{

}




