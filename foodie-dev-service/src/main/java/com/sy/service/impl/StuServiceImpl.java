package com.sy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.pojo.Stu;
import com.sy.service.StuService;
import com.sy.mapper.StuMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【stu】的数据库操作Service实现
* @createDate 2021-11-03 22:10:41
*/
@Service
public class StuServiceImpl extends ServiceImpl<StuMapper, Stu>
    implements StuService{

}




