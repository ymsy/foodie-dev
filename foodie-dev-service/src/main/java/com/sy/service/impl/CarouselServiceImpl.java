package com.sy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.pojo.Carousel;
import com.sy.service.CarouselService;
import com.sy.mapper.CarouselMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @description 针对表【carousel(轮播图 )】的数据库操作Service实现
 * @createDate 2021-11-03 22:10:41
 */
@Service
public class CarouselServiceImpl extends ServiceImpl<CarouselMapper, Carousel> implements CarouselService {

    @Override
    public List<Carousel> queryAll(Integer isShow) {
        return list(
                new QueryWrapper<Carousel>()
                        .lambda()
                        .eq(null != isShow, Carousel::getIsShow, isShow)
                        .orderByAsc(Carousel::getSort)
        );
    }
}




