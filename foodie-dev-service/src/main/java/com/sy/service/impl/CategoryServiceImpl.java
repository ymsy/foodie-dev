package com.sy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.mapper.CategoryMapper;
import com.sy.pojo.Category;
import com.sy.service.CategoryService;
import com.sy.vo.CategoryVO;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @description 针对表【category(商品分类 )】的数据库操作Service实现
 * @createDate 2021-11-03 22:10:41
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Override
    public List<Category> queryAllRootLevelCat() {
        return list(
                new QueryWrapper<Category>()
                        .lambda()
                        .eq(Category::getType, 1)
        );
    }

    @Override
    public List<CategoryVO> querySubCatList(Integer rootCatId) {
        return baseMapper.getSubCatList(rootCatId);
    }

    @Override
    public List querySixNewItemsLazy(Integer rootCatId) {
        Map<String, Object> map = new HashMap<>();
        map.put("rootCatId", rootCatId);
        return baseMapper.getSixNewItemsLazy(map);
    }
}




