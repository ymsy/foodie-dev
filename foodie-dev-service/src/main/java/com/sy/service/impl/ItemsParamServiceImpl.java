package com.sy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.pojo.ItemsParam;
import com.sy.service.ItemsParamService;
import com.sy.mapper.ItemsParamMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【items_param(商品参数 )】的数据库操作Service实现
* @createDate 2021-11-03 22:10:41
*/
@Service
public class ItemsParamServiceImpl extends ServiceImpl<ItemsParamMapper, ItemsParam>
    implements ItemsParamService{

}




