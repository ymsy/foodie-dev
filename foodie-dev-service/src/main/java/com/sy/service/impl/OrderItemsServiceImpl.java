package com.sy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.pojo.OrderItems;
import com.sy.service.OrderItemsService;
import com.sy.mapper.OrderItemsMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【order_items(订单商品关联表 )】的数据库操作Service实现
* @createDate 2021-11-03 22:10:41
*/
@Service
public class OrderItemsServiceImpl extends ServiceImpl<OrderItemsMapper, OrderItems>
    implements OrderItemsService{

}




