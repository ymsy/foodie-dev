package com.sy.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.mapper.ItemsCommentsMapper;
import com.sy.pojo.ItemsComments;
import com.sy.service.ItemsCommentsService;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @description 针对表【items_comments(商品评价表 )】的数据库操作Service实现
 * @createDate 2021-11-03 22:10:41
 */
@Service
public class ItemsCommentsServiceImpl extends ServiceImpl<ItemsCommentsMapper, ItemsComments>
    implements ItemsCommentsService{

}




