package com.sy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.enums.YesOrNo;
import com.sy.mapper.ItemsImgMapper;
import com.sy.pojo.ItemsImg;
import com.sy.service.ItemsImgService;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【items_img(商品图片 )】的数据库操作Service实现
* @createDate 2021-11-03 22:10:41
*/
@Service
public class ItemsImgServiceImpl extends ServiceImpl<ItemsImgMapper, ItemsImg> implements ItemsImgService{

    @Override
    public String queryItemMainImgById(String itemId) {
        ItemsImg itemsImg = new ItemsImg();
        itemsImg.setItemId(itemId);
        itemsImg.setIsMain(YesOrNo.YES.type);
        ItemsImg result = baseMapper.selectOne(new QueryWrapper<>(itemsImg));
        return result != null ? result.getUrl() : "";
    }
}




