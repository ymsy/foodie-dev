package com.sy.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.bo.AddressBO;
import com.sy.enums.YesOrNo;
import com.sy.mapper.UserAddressMapper;
import com.sy.pojo.UserAddress;
import com.sy.service.UserAddressService;
import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Administrator
 * @description 针对表【user_address(用户地址表 )】的数据库操作Service实现
 * @createDate 2021-11-03 22:10:41
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements UserAddressService {

    @Autowired
    private Sid sid;

    @Override
    public List<UserAddress> queryAll(String userId) {
        return list(
                new QueryWrapper<UserAddress>()
                        .lambda()
                        .eq(UserAddress::getUserId, userId)
        );
    }

    @Override
    public void addNewUserAddress(AddressBO addressBO) {
        // 1. 判断当前用户是否存在地址，如果没有，则新增为'默认地址'
        Integer isDefault = 0;
        List<UserAddress> addressList = this.queryAll(addressBO.getUserId());
        if (CollUtil.isEmpty(addressList)) {
            isDefault = 1;
        }
        String addressId = sid.nextShort();
        UserAddress newAddress = Convert.convert(UserAddress.class, addressBO);
        newAddress.setId(addressId);
        newAddress.setIsDefault(isDefault);
        baseMapper.insert(newAddress);

    }

    @Override
    public void updateUserAddress(AddressBO addressBO) {
        String addressId = addressBO.getAddressId();
        UserAddress pendingAddress = Convert.convert(UserAddress.class, addressBO);
        pendingAddress.setId(addressId);
        baseMapper.updateById(pendingAddress);
    }

    @Override
    public void deleteUserAddress(String userId, String addressId) {
        baseMapper.delete(
                new QueryWrapper<UserAddress>()
                        .lambda()
                        .eq(UserAddress::getUserId, userId)
                        .eq(UserAddress::getId, addressId)

        );
    }


    @Transactional
    @Override
    public void updateUserAddressToBeDefault(String userId, String addressId) {
        List<UserAddress> list = baseMapper.selectList(
                new QueryWrapper<UserAddress>()
                        .lambda()
                        .eq(UserAddress::getUserId, userId)
                        .eq(UserAddress::getIsDefault, YesOrNo.YES.type)
        );
        for (UserAddress ua : list) {
            ua.setIsDefault(YesOrNo.NO.type);
            baseMapper.updateById(ua);
        }
        UserAddress defaultAddress = new UserAddress();
        defaultAddress.setId(addressId);
        defaultAddress.setUserId(userId);
        defaultAddress.setIsDefault(YesOrNo.YES.type);
        baseMapper.updateById(defaultAddress);
    }

    @Override
    public UserAddress queryUserAddress(String userId, String addressId) {
        return baseMapper.selectOne(
                new QueryWrapper<UserAddress>()
                        .lambda()
                        .eq(UserAddress::getUserId, userId)
                        .eq(UserAddress::getId, addressId)
        );
    }
}




