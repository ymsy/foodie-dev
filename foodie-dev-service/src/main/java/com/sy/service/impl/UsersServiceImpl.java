package com.sy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.bo.UserBO;
import com.sy.enums.Sex;
import com.sy.mapper.UsersMapper;
import com.sy.pojo.Users;
import com.sy.service.UsersService;
import com.sy.util.DateUtil;
import com.sy.util.MD5Util;
import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @description 针对表【users(用户表 )】的数据库操作Service实现
 * @createDate 2021-11-08 22:19:37
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {

    public static final String USER_FACE = "http://122.152.205.72:88/group1/M00/00/05/CpoxxFw_8_qAIlFXAAAcIhVPdSg994.png";

    @Autowired
    private Sid sid;

    @Override
    public boolean queryUsernameIsExist(String username) {
        Users user = baseMapper.selectOne(
                new QueryWrapper<Users>()
                        .lambda()
                        .eq(Users::getUsername, username)
        );
        return user != null;
    }

    @Override
    public Users addUser(UserBO userBO) throws Exception {
        Users user = new Users();
        user.setId(sid.nextShort());
        user.setUsername(userBO.getUsername());
        user.setPassword(MD5Util.getMD5Str(userBO.getPassword()));
        user.setNickname(userBO.getUsername());
        user.setFace(USER_FACE);
        user.setBirthday(DateUtil.convertToDate("1900-01-01"));
        user.setSex(Sex.secret.type);
        baseMapper.insert(user);
        return user;
    }

    @Override
    public Users queryUserForLogin(String username, String password) throws Exception {
        Users user = baseMapper.selectOne(
                new QueryWrapper<Users>()
                        .lambda()
                        .eq(Users::getUsername, username)
                        .eq(Users::getPassword, MD5Util.getMD5Str(password))
        );
        return user;
    }
}




