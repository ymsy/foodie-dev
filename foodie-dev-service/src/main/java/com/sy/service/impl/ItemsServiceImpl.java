package com.sy.service.impl;

import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.enums.CommentLevel;
import com.sy.mapper.*;
import com.sy.pojo.*;
import com.sy.service.ItemsService;
import com.sy.util.DesensitizationUtil;
import com.sy.vo.CommentsLevelCountsVO;
import com.sy.vo.ItemCommentVO;
import com.sy.vo.ItemInfoVO;
import com.sy.vo.ShopcartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Administrator
 * @description 针对表【items(商品表 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表)】的数据库操作Service实现
 * @createDate 2021-11-03 22:10:41
 */
@Service
public class ItemsServiceImpl extends ServiceImpl<ItemsMapper, Items> implements ItemsService {

    @Autowired
    private ItemsImgMapper itemsImgMapper;
    @Autowired
    private ItemsSpecMapper itemsSpecMapper;
    @Autowired
    private ItemsParamMapper itemsParamMapper;
    @Autowired
    private ItemsCommentsMapper itemsCommentsMapper;

    @Override
    public ItemInfoVO queryInfo(String itemId) {
        Items items = getById(itemId);
        List<ItemsImg> itemsImgList = itemsImgMapper.selectList(
                new QueryWrapper<ItemsImg>()
                        .lambda()
                        .eq(ItemsImg::getItemId, itemId)
        );
        List<ItemsSpec> itemsSpecList = itemsSpecMapper.selectList(
                new QueryWrapper<ItemsSpec>()
                        .lambda()
                        .eq(ItemsSpec::getItemId, itemId)
        );
        ItemsParam itemsParam = itemsParamMapper.selectOne(
                new QueryWrapper<ItemsParam>()
                        .lambda()
                        .eq(ItemsParam::getItemId, itemId)
        );
        ItemInfoVO itemInfoVO = new ItemInfoVO();
        itemInfoVO.setItem(items);
        itemInfoVO.setItemImgList(itemsImgList);
        itemInfoVO.setItemSpecList(itemsSpecList);
        itemInfoVO.setItemParams(itemsParam);
        return itemInfoVO;
    }

    @Override
    public CommentsLevelCountsVO queryCommentsCounts(String itemId) {
        Integer goodCounts = getCommentsCounts(itemId, CommentLevel.GOOD.type);
        Integer normalCounts = getCommentsCounts(itemId, CommentLevel.NORMAL.type);
        Integer badCounts = getCommentsCounts(itemId, CommentLevel.BAD.type);
        Integer totalCounts = goodCounts + normalCounts + badCounts;
        CommentsLevelCountsVO countsVO = new CommentsLevelCountsVO();
        countsVO.setGoodCounts(goodCounts);
        countsVO.setNormalCounts(normalCounts);
        countsVO.setBadCounts(badCounts);
        countsVO.setTotalCounts(totalCounts);
        return countsVO;
    }

    @Override
    public IPage queryCommentsPage(Page page, String itemId, Integer level) {
        Map<String, Object> map = new HashMap<>();
        map.put("itemId", itemId);
        map.put("level", level);
        IPage<ItemCommentVO> result = itemsCommentsMapper.getPage(page, map);
        result.getRecords().forEach(e -> {
            e.setNickname(DesensitizationUtil.commonDisplay(e.getNickname()));
        });
        return result;
    }

    Integer getCommentsCounts(String itemId, Integer level) {
        return itemsCommentsMapper.selectCount(
                new QueryWrapper<ItemsComments>()
                        .lambda()
                        .eq(null != itemId, ItemsComments::getItemId, itemId)
                        .eq(null != level, ItemsComments::getCommentLevel, level)
        ).intValue();
    }

    @Override
    public IPage searchItems(Page page, String keywords, String sort) {
        Map<String, Object> map = new HashMap<>();
        map.put("keywords", keywords);
        map.put("sort", sort);
        return baseMapper.searchItems(page, map);
    }

    @Override
    public IPage searchItems(Page page, Integer catId, String sort) {
        Map<String, Object> map = new HashMap<>();
        map.put("catId", catId);
        map.put("sort", sort);
        return baseMapper.searchItemsByThirdCat(page, map);
    }

    @Override
    public List<ShopcartVO> queryItemsBySpecIds(String specIds) {
        String[] ids = specIds.split(",");
        List<String> specIdsList = ListUtil.toList(ids);
        return baseMapper.queryItemsBySpecIds(specIdsList);
    }

    @Override
    public void decreaseItemSpecStock(String specId, int buyCounts) {

    }
}




