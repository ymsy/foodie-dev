package com.sy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sy.pojo.Carousel;

import java.util.List;

/**
* @author Administrator
* @description 针对表【carousel(轮播图 )】的数据库操作Service
* @createDate 2021-11-03 22:10:41
*/
public interface CarouselService extends IService<Carousel> {

    List<Carousel> queryAll(Integer isShow);


}
