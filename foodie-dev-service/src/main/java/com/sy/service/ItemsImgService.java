package com.sy.service;

import com.sy.pojo.ItemsImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【items_img(商品图片 )】的数据库操作Service
* @createDate 2021-11-03 22:10:41
*/
public interface ItemsImgService extends IService<ItemsImg> {

    String queryItemMainImgById(String itemId);

}
