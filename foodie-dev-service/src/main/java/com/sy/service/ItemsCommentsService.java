package com.sy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sy.pojo.ItemsComments;

/**
* @author Administrator
* @description 针对表【items_comments(商品评价表 )】的数据库操作Service
* @createDate 2021-11-03 22:10:41
*/
public interface ItemsCommentsService extends IService<ItemsComments> {

}
