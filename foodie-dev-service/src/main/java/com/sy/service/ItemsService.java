package com.sy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sy.pojo.Items;
import com.sy.vo.CommentsLevelCountsVO;
import com.sy.vo.ItemInfoVO;
import com.sy.vo.ShopcartVO;

import java.util.List;

/**
 * @author Administrator
 * @description 针对表【items(商品表 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表)】的数据库操作Service
 * @createDate 2021-11-03 22:10:41
 */
public interface ItemsService extends IService<Items> {

    ItemInfoVO queryInfo(String itemId);

    CommentsLevelCountsVO queryCommentsCounts(String itemId);

    IPage queryCommentsPage(Page page, String itemId, Integer level);

    IPage searchItems(Page page, String keywords, String sort);

    IPage searchItems(Page page, Integer catId, String sort);

    List<ShopcartVO> queryItemsBySpecIds(String specIds);

    /**
     * 减少库存
     *
     * @param specId
     * @param buyCounts
     */
    void decreaseItemSpecStock(String specId, int buyCounts);
}
