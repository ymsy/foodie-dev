package com.sy.service;

import com.sy.pojo.Stu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【stu】的数据库操作Service
* @createDate 2021-11-03 22:10:41
*/
public interface StuService extends IService<Stu> {

}
