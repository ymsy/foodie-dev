package com.sy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sy.pojo.Category;
import com.sy.vo.CategoryVO;

import java.util.List;

/**
 * @author Administrator
 * @description 针对表【category(商品分类 )】的数据库操作Service
 * @createDate 2021-11-03 22:10:41
 */
public interface CategoryService extends IService<Category> {

    List<Category> queryAllRootLevelCat();

    List<CategoryVO> querySubCatList(Integer rootCatId);

    List querySixNewItemsLazy(Integer rootCatId);

}
