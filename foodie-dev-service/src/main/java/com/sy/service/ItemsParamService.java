package com.sy.service;

import com.sy.pojo.ItemsParam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【items_param(商品参数 )】的数据库操作Service
* @createDate 2021-11-03 22:10:41
*/
public interface ItemsParamService extends IService<ItemsParam> {

}
