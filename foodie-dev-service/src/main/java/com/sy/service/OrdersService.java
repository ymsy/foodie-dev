package com.sy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sy.bo.ShopcartBO;
import com.sy.bo.SubmitOrderBO;
import com.sy.pojo.Orders;
import com.sy.vo.OrderVO;

import java.util.List;

/**
 * @author Administrator
 * @description 针对表【orders(订单表;)】的数据库操作Service
 * @createDate 2021-11-03 22:10:41
 */
public interface OrdersService extends IService<Orders> {

    /**
     * 用于创建订单相关信息
     *
     * @param submitOrderBO
     */
    OrderVO createOrder(List<ShopcartBO> shopcartList, SubmitOrderBO submitOrderBO);

}
