package com.sy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sy.bo.UserBO;
import com.sy.pojo.Users;

/**
 * @author Administrator
 * @description 针对表【users(用户表 )】的数据库操作Service
 * @createDate 2021-11-08 22:19:37
 */
public interface UsersService extends IService<Users> {

    boolean queryUsernameIsExist(String username);

    Users addUser(UserBO userBO) throws Exception;

    Users queryUserForLogin(String username, String password) throws Exception;

}
