package com.sy.service;

import com.sy.bo.AddressBO;
import com.sy.pojo.UserAddress;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Administrator
 * @description 针对表【user_address(用户地址表 )】的数据库操作Service
 * @createDate 2021-11-03 22:10:41
 */
public interface UserAddressService extends IService<UserAddress> {

    List<UserAddress> queryAll(String userId);

    void addNewUserAddress(AddressBO addressBO);

    void updateUserAddress(AddressBO addressBO);

    void deleteUserAddress(String userId, String addressId);

    void updateUserAddressToBeDefault(String userId, String addressId);

    UserAddress queryUserAddress(String userId, String addressId);
}
