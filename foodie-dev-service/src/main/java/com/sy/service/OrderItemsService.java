package com.sy.service;

import com.sy.pojo.OrderItems;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【order_items(订单商品关联表 )】的数据库操作Service
* @createDate 2021-11-03 22:10:41
*/
public interface OrderItemsService extends IService<OrderItems> {

}
