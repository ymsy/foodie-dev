package com.sy.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserBO {

    @ApiModelProperty(value = "用户名", required = true)
    private String username;

    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @ApiModelProperty(value = "确认密码", required = false)
    private String confirmPassword;


}
