package com.sy.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表
 *
 * @TableName users
 */
@TableName(value = "users")
@Data
public class Users implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 主键id 用户id
     */
    @TableId
    @ApiModelProperty("主键id 用户id")
    private String id;

    /**
     * 用户名 用户名
     */
    @ApiModelProperty("用户名 用户名")
    private String username;

    /**
     * 密码 密码
     */
    @ApiModelProperty("密码 密码")
    @JsonIgnore
    private String password;

    /**
     * 昵称 昵称
     */
    @ApiModelProperty("昵称 昵称")
    private String nickname;

    /**
     * 真实姓名
     */
    @ApiModelProperty("真实姓名")
    private String realname;

    /**
     * 头像
     */
    @ApiModelProperty("头像")
    private String face;

    /**
     * 手机号 手机号
     */
    @ApiModelProperty("手机号 手机号")
    private String mobile;

    /**
     * 邮箱地址 邮箱地址
     */
    @ApiModelProperty("邮箱地址 邮箱地址")
    private String email;

    /**
     * 性别 性别 1:男  0:女  2:保密
     */
    @ApiModelProperty("性别 性别 1:男  0:女  2:保密")
    private Integer sex;

    /**
     * 生日 生日
     */
    @ApiModelProperty("生日 生日")
    private Date birthday;

    /**
     * 创建时间 创建时间
     */
    @ApiModelProperty("创建时间 创建时间")
    private Date createdTime;

    /**
     * 更新时间 更新时间
     */
    @ApiModelProperty("更新时间 更新时间")
    private Date updatedTime;

    public Users setNullProperty() {
        this.password = null;
        this.mobile = null;
        this.email = null;
        this.createdTime = null;
        this.updatedTime = null;
        this.birthday = null;
        return this;
    }
}
