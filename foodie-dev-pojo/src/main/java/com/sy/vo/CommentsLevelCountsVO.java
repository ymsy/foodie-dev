package com.sy.vo;

import lombok.Data;

@Data
public class CommentsLevelCountsVO {

    private Integer totalCounts;
    private Integer goodCounts;
    private Integer normalCounts;
    private Integer badCounts;

}
