package com.sy.vo;

import com.sy.pojo.Items;
import com.sy.pojo.ItemsImg;
import com.sy.pojo.ItemsParam;
import com.sy.pojo.ItemsSpec;
import lombok.Data;

import java.util.List;

@Data
public class ItemInfoVO {

    private Items item;
    private List<ItemsImg> itemImgList;
    private List<ItemsSpec> itemSpecList;
    private ItemsParam itemParams;

}
