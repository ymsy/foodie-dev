package com.sy.mapper;

import com.sy.pojo.OrderItems;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【order_items(订单商品关联表 )】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.OrderItems
*/
public interface OrderItemsMapper extends BaseMapper<OrderItems> {

}




