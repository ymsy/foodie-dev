package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sy.pojo.Items;
import com.sy.vo.SearchItemsVO;
import com.sy.vo.ShopcartVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @description 针对表【items(商品表 商品信息相关表：分类表，商品图片表，商品规格表，商品参数表)】的数据库操作Mapper
 * @createDate 2021-11-03 22:10:41
 * @Entity com.sy.pojo.Items
 */
public interface ItemsMapper extends BaseMapper<Items> {

    IPage<SearchItemsVO> searchItems(Page page, @Param("paramsMap") Map<String, Object> map);

    IPage<SearchItemsVO> searchItemsByThirdCat(Page page, @Param("paramsMap") Map<String, Object> map);

    List<ShopcartVO> queryItemsBySpecIds(@Param("paramsList") List specIdsList);

}




