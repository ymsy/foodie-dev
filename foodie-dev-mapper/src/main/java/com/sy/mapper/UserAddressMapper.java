package com.sy.mapper;

import com.sy.pojo.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【user_address(用户地址表 )】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.UserAddress
*/
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}




