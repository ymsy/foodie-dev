package com.sy.mapper;

import com.sy.pojo.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【orders(订单表;)】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.Orders
*/
public interface OrdersMapper extends BaseMapper<Orders> {

}




