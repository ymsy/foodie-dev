package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sy.pojo.ItemsComments;
import com.sy.vo.ItemCommentVO;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * @author Administrator
 * @description 针对表【items_comments(商品评价表 )】的数据库操作Mapper
 * @createDate 2021-11-03 22:10:41
 * @Entity com.sy.pojo.ItemsComments
 */
public interface ItemsCommentsMapper extends BaseMapper<ItemsComments> {

    IPage<ItemCommentVO> getPage(Page page, @Param("paramsMap") Map<String, Object> map);

}




