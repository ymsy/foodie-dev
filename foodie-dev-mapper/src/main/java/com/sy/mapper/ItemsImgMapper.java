package com.sy.mapper;

import com.sy.pojo.ItemsImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【items_img(商品图片 )】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.ItemsImg
*/
public interface ItemsImgMapper extends BaseMapper<ItemsImg> {

}




