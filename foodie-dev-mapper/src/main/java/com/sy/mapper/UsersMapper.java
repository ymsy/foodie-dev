package com.sy.mapper;

import com.sy.pojo.Users;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【users(用户表 )】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.Users
*/
public interface UsersMapper extends BaseMapper<Users> {

}




