package com.sy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.pojo.Category;
import com.sy.vo.CategoryVO;
import com.sy.vo.NewItemsVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @description 针对表【category(商品分类 )】的数据库操作Mapper
 * @createDate 2021-11-03 22:10:41
 * @Entity com.sy.pojo.Category
 */
public interface CategoryMapper extends BaseMapper<Category> {

    List<CategoryVO> getSubCatList(@Param("rootCatId") Integer rootCatId);

    List<NewItemsVO> getSixNewItemsLazy(@Param("paramsMap") Map<String, Object> map);

}




