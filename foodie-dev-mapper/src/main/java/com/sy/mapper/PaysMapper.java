package com.sy.mapper;

import com.sy.pojo.Pays;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【pays(支付表)】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.Pays
*/
public interface PaysMapper extends BaseMapper<Pays> {

}




