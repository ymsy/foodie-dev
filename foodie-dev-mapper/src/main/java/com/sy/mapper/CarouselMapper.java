package com.sy.mapper;

import com.sy.pojo.Carousel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【carousel(轮播图 )】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.Carousel
*/
public interface CarouselMapper extends BaseMapper<Carousel> {

}




