package com.sy.mapper;

import com.sy.pojo.ItemsSpec;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【items_spec(商品规格 每一件商品都有不同的规格，不同的规格又有不同的价格和优惠力度，规格表为此设计)】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.ItemsSpec
*/
public interface ItemsSpecMapper extends BaseMapper<ItemsSpec> {

}




