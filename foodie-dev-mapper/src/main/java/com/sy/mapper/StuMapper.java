package com.sy.mapper;

import com.sy.pojo.Stu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【stu】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.Stu
*/
public interface StuMapper extends BaseMapper<Stu> {

}




