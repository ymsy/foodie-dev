package com.sy.mapper;

import com.sy.pojo.ItemsParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【items_param(商品参数 )】的数据库操作Mapper
* @createDate 2021-11-03 22:10:41
* @Entity com.sy.pojo.ItemsParam
*/
public interface ItemsParamMapper extends BaseMapper<ItemsParam> {

}




