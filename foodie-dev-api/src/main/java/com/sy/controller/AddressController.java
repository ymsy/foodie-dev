package com.sy.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.sy.bo.AddressBO;
import com.sy.service.UserAddressService;
import com.sy.util.MobileEmailUtils;
import com.sy.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "地址相关")
@RequestMapping("address")
@RestController
public class AddressController {

    @Autowired
    private UserAddressService addressService;


    @ApiOperation(value = "根据用户id查询收货地址列表")
    @PostMapping("/list")
    public Result list(@RequestParam String userId) {
        if (StringUtils.isBlank(userId)) {
            return Result.errorMsg(null);
        }
        return Result.ok(addressService.queryAll(userId));
    }

    @ApiOperation(value = "用户新增地址", notes = "用户新增地址", httpMethod = "POST")
    @PostMapping("/add")
    public Result add(@RequestBody AddressBO addressBO) {
        Result checkRes = checkAddress(addressBO);
        if (checkRes.getStatus() != 200) {
            return checkRes;
        }
        addressService.addNewUserAddress(addressBO);
        return Result.ok();
    }

    @ApiOperation(value = "用户修改地址")
    @PostMapping("/update")
    public Result update(@RequestBody AddressBO addressBO) {
        if (StrUtil.isBlank(addressBO.getAddressId())) {
            return Result.errorMsg("修改地址错误：addressId不能为空");
        }
        Result checkRes = checkAddress(addressBO);
        if (checkRes.getStatus() != 200) {
            return checkRes;
        }
        addressService.updateUserAddress(addressBO);
        return Result.ok();
    }

    @ApiOperation(value = "用户删除地址")
    @PostMapping("/delete")
    public Result delete(@RequestParam String userId,
                         @RequestParam String addressId) {
        if (StrUtil.isBlank(userId) || StrUtil.isBlank(addressId)) {
            return Result.errorMsg(null);
        }
        addressService.deleteUserAddress(userId, addressId);
        return Result.ok();
    }


    private Result checkAddress(AddressBO addressBO) {
        String receiver = addressBO.getReceiver();
        if (StringUtils.isBlank(receiver)) {
            return Result.errorMsg("收货人不能为空");
        }
        if (receiver.length() > 12) {
            return Result.errorMsg("收货人姓名不能太长");
        }

        String mobile = addressBO.getMobile();
        if (StringUtils.isBlank(mobile)) {
            return Result.errorMsg("收货人手机号不能为空");
        }
        if (mobile.length() != 11) {
            return Result.errorMsg("收货人手机号长度不正确");
        }
        boolean isMobileOk = MobileEmailUtils.checkMobileIsOk(mobile);
        if (!isMobileOk) {
            return Result.errorMsg("收货人手机号格式不正确");
        }

        String province = addressBO.getProvince();
        String city = addressBO.getCity();
        String district = addressBO.getDistrict();
        String detail = addressBO.getDetail();
        if (StringUtils.isBlank(province) ||
                StringUtils.isBlank(city) ||
                StringUtils.isBlank(district) ||
                StringUtils.isBlank(detail)) {
            return Result.errorMsg("收货地址信息不能为空");
        }
        return Result.ok();
    }

    @ApiOperation(value = "用户设置默认地址")
    @PostMapping("/setDefalut")
    public Result setDefalut(@RequestParam String userId,
                             @RequestParam String addressId) {
        if (StrUtil.isBlank(userId) || StrUtil.isBlank(addressId)) {
            return Result.errorMsg(null);
        }
        addressService.updateUserAddressToBeDefault(userId, addressId);
        return Result.ok();
    }
}
