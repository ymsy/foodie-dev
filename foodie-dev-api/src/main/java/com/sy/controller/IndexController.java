package com.sy.controller;

import com.sy.enums.YesOrNo;
import com.sy.service.CarouselService;
import com.sy.service.CategoryService;
import com.sy.util.Result;
import com.sy.vo.NewItemsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "首页", tags = {"首页展示的相关接口"})
@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private CarouselService carouselService;

    @Autowired
    private CategoryService categoryService;

    @ApiOperation(value = "查询首页轮播图")
    @GetMapping("/carousel")
    public Result carousel() {
        return Result.ok(carouselService.queryAll(YesOrNo.YES.type));
    }

    @ApiOperation(value = "获取商品分类(一级)")
    @GetMapping("/cats")
    public Result cats() {
        return Result.ok(categoryService.queryAllRootLevelCat());
    }

    @ApiOperation(value = "获取商品子分类")
    @GetMapping("/subCat/{rootCatId}")
    public Result subCat(@PathVariable Integer rootCatId) {
        if (null == rootCatId) {
            return Result.errorMsg("分类不存在");
        }
        return Result.ok(categoryService.querySubCatList(rootCatId));
    }

    @ApiOperation(value = "查询每个一级分类下的最新6条商品数据")
    @GetMapping("/sixNewItems/{rootCatId}")
    public Result sixNewItems(@PathVariable Integer rootCatId) {
        if (null == rootCatId) {
            return Result.errorMsg("分类不存在");
        }
        List<NewItemsVO> list = categoryService.querySixNewItemsLazy(rootCatId);
        return Result.ok(list);
    }
}
