package com.sy.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.sy.bo.ShopcartBO;
import com.sy.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "购物车接口controller")
@RestController
@RequestMapping("/shopcart")
public class ShopcartController extends BaseController {


    @ApiOperation(value = "添加商品到购物车", notes = "添加商品到购物车", httpMethod = "POST")
    @PostMapping("/add")
    public Result add(@RequestParam String userId,
                      @RequestBody ShopcartBO shopcartBO,
                      HttpServletRequest request,
                      HttpServletResponse response) {
        if (StringUtils.isBlank(userId)) {
            return Result.errorMsg(null);
        }
        //TODO 前端用户在登录的情况下，添加商品到购物车，会同时在后端同步购物车到redis缓存
        return Result.ok();
    }

    @ApiOperation(value = "从购物车中删除商品")
    @PostMapping("/del")
    public Result del(@RequestParam String userId,
                      @RequestParam String itemSpecId,
                      HttpServletRequest request,
                      HttpServletResponse response) {
        if (StrUtil.isBlank(userId) || StrUtil.isBlank(itemSpecId)) {
            return Result.errorMsg("参数不能为空");
        }
        //TODO 用户在页面删除购物车中的商品数据，如果此时用户已经登录，则需要同步删除后端购物车中的商品
        return Result.ok();
    }

}
