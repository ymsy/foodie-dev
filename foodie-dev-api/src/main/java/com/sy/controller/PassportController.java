package com.sy.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.sy.bo.UserBO;
import com.sy.pojo.Users;
import com.sy.service.UsersService;
import com.sy.util.CookieUtil;
import com.sy.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "注册登录", tags = {"注册登录接口"})
@RestController
@RequestMapping("/passport")
public class PassportController {

    @Autowired
    private UsersService usersService;

    @ApiOperation(value = "查询用户名是否存在")
    @GetMapping("/usernameIsExist")
    public Result usernameIsExist(@RequestParam String username) {
        if (StrUtil.isBlank(username)) {
            return Result.errorMsg("用户名不能为空");
        }
        boolean isExist = usersService.queryUsernameIsExist(username);
        if (isExist) {
            return Result.errorMsg("用户名已经存在");
        }
        return Result.ok();
    }

    @ApiOperation(value = "用户注册")
    @PostMapping("/regist")
    public Result regist(@RequestBody UserBO bo) throws Exception {
        String password = bo.getPassword();
        String username = bo.getUsername();
        String confirmPassword = bo.getConfirmPassword();
        if (StrUtil.isNotBlank(password) && StrUtil.isNotBlank(username)) {
            if (usersService.queryUsernameIsExist(username)) {
                return Result.errorMsg("用户名已存在");
            }
            if (!password.equals(confirmPassword)) {
                return Result.errorMsg("两次输入密码不一致");
            }
            if (password.length() < 6) {
                return Result.errorMsg("密码长度不能少于6");
            }
        } else {
            return Result.errorMsg("用户名或密码不能为空");
        }
        usersService.addUser(bo);
        return Result.ok();
    }

    @ApiOperation(value = "用户登录")
    @PostMapping("/login")
    public Result login(@RequestBody UserBO bo,
                        HttpServletRequest request,
                        HttpServletResponse response) throws Exception {
        String password = bo.getPassword();
        String username = bo.getUsername();
        if (StrUtil.isNotBlank(password) && StrUtil.isNotBlank(username)) {
            Users user = usersService.queryUserForLogin(bo.getUsername(), bo.getPassword());
            if (null == user) {
                return Result.errorMsg("用户名或密码不正确");
            }
            CookieUtil.setCookie(request, response, "user", JSONUtil.toJsonStr(user), true);
            return Result.ok(user.setNullProperty());
        } else {
            return Result.errorMsg("用户名或密码不能为空");
        }

    }

    @ApiOperation(value = "用户退出登录")
    @PostMapping("/logout")
    public Result logout(@RequestParam String userId,
                         HttpServletRequest request,
                         HttpServletResponse response) throws Exception {
        CookieUtil.deleteCookie(request, response, "user");
        return Result.ok();
    }

}
