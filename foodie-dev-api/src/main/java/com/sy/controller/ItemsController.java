package com.sy.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sy.service.ItemsService;
import com.sy.util.PagedGridResult;
import com.sy.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "商品接口", tags = {"商品信息展示的相关接口"})
@RestController
@RequestMapping("items")
public class ItemsController extends BaseController {

    @Autowired
    private ItemsService itemsService;

    @ApiOperation(value = "查询商品详情")
    @GetMapping("/info/{itemId}")
    public Result info(@PathVariable String itemId) {
        if (StrUtil.isBlank(itemId)) {
            return Result.errorMsg("商品不存在");
        }
        return Result.ok(itemsService.queryInfo(itemId));
    }

    @ApiOperation(value = "查询商品评价等级")
    @GetMapping("/commentLevel")
    public Result commentLevel(@RequestParam String itemId) {
        if (StrUtil.isBlank(itemId)) {
            return Result.errorMsg("商品不存在");
        }
        return Result.ok(itemsService.queryCommentsCounts(itemId));
    }

    @ApiOperation(value = "查询商品评论")
    @GetMapping("/comments")
    public Result comments(@RequestParam String itemId,
                           @RequestParam Integer level,
                           @RequestParam(required = false, defaultValue = "1") Integer page,
                           @RequestParam(required = false, defaultValue = COMMON_PAGE_SIZE) Integer pageSize) {
        if (StrUtil.isBlank(itemId)) {
            return Result.errorMsg("商品不存在");
        }
        return Result.ok(new PagedGridResult(itemsService.queryCommentsPage(new Page(page, pageSize), itemId, level)));
    }

    @ApiOperation(value = "搜索商品列表")
    @GetMapping("/search")
    public Result search(
            @RequestParam String keywords,
            @RequestParam String sort,
            @RequestParam(required = false, defaultValue = "1") Integer page,
            @RequestParam(required = false, defaultValue = COMMON_PAGE_SIZE) Integer pageSize) {

        if (StrUtil.isBlank(keywords)) {
            return Result.errorMsg(null);
        }
        return Result.ok(new PagedGridResult(itemsService.searchItems(new Page(page, pageSize), keywords, sort)));
    }

    @ApiOperation(value = "通过分类id搜索商品列表")
    @GetMapping("/catItems")
    public Result catItems(@RequestParam Integer catId,
                           @RequestParam String sort,
                           @RequestParam(required = false, defaultValue = "1") Integer page,
                           @RequestParam(required = false, defaultValue = COMMON_PAGE_SIZE) Integer pageSize) {
        if (catId == null) {
            return Result.errorMsg(null);
        }
        return Result.ok(new PagedGridResult(itemsService.searchItems(new Page(page, pageSize), catId, sort)));
    }

    @ApiOperation(value = "根据商品规格ids查找最新的商品数据")
    @GetMapping("/refresh")
    public Result refresh(@RequestParam String itemSpecIds) {
        if (StringUtils.isBlank(itemSpecIds)) {
            return Result.ok();
        }
        return Result.ok(itemsService.queryItemsBySpecIds(itemSpecIds));
    }


}
