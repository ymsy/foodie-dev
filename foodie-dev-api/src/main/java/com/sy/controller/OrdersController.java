package com.sy.controller;


import com.sy.bo.SubmitOrderBO;
import com.sy.enums.PayMethod;
import com.sy.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "订单相关", tags = {"订单相关的api接口"})
@RequestMapping("/orders")
@RestController
public class OrdersController {

    @ApiOperation(value = "用户下单")
    @PostMapping("/create")
    public Result create(@RequestBody SubmitOrderBO submitOrderBO,
                         HttpServletRequest request,
                         HttpServletResponse response) {
        if (submitOrderBO.getPayMethod() != PayMethod.WEIXIN.type
                && submitOrderBO.getPayMethod() != PayMethod.ALIPAY.type) {
            return Result.errorMsg("支付方式不支持!");
        }

        return Result.ok();
    }

}
