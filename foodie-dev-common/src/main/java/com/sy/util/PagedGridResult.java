package com.sy.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.util.List;

@Data
public class PagedGridResult {

    private long page;            // 当前页数
    private long total;            // 总页数
    private long records;        // 总记录数
    private List<?> rows;

    public PagedGridResult(IPage<?> page) {
        this.page = page.getCurrent();
        this.rows = page.getRecords();
        this.total = page.getPages();
        this.records = page.getTotal();
    }
}
